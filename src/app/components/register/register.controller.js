(function() {
  'use strict';

  angular
    .module('webapp')
    .controller('RegisterController', RegisterController);

  /** @ngInject */
  function RegisterController() {
    var vm = this;
    vm.business = {};
    vm.agent = {};

    vm.business.name = 'Doggy Walker';

    vm.analysis = function(){
        var amount = vm.business.amountRequested;

        swal({title: "Are you sure?",
              text: "You will not be able to change your information!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, send information!",
              cancelButtonText: "No, I want review my information",
              closeOnConfirm: false,
              closeOnCancel: false }, function(isConfirm){
                if(isConfirm && Number.isInteger(amount)){
                    var response;
                    if( amount > 50000){
                        response = " Declined.";
                    }
                    else{
                        response = " Approved.";
                        if(amount == 50000){
                            response = " Undecided.";
                        }
                    }
                  swal("Information!", "Dear " + vm.business.name +" your loan application has been " + response, "warning");
                }
                else {
                    swal("Cancelled", "Review your information!", "error");
                }
            });

    }
  }
})();