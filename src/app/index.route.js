(function() {
  'use strict';

  angular
    .module('webapp')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/register', {
        templateUrl : 'app/components/register/register.html',
        controller : 'RegisterController',
        controllerAs: 'register'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
